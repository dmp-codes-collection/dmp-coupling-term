%%
clc
clear all
close all

%% encode into the trajectory into DMPs
% Load the demonstrated trajectory 
load Example_Demostration.mat
% ppath - recording of cathesian space tool positions
% fpath - force profile associated with tool space positions


% setup DMP parameters
dt=0.01;
pDMP.N = 100;
pDMP.dt=dt;
pDMP.a_z=48;
pDMP.a_x=2;
RBF.N= 100;
RBF.dt=dt;
RBF.a_z=48;
RBF.a_x=2;

%encode force profile with Radial Basis Functions
RBF=RBF_encode(fpath,RBF);

% Carthesian position DMP (pDMP)
pDMP=pDMPlearn(ppath,pDMP);

%% Prepare parameters for integration
%% Calculate final phase for the DMP integration
T_f = (length(ppath)+1)*pDMP.dt;
Xmin = exp(-pDMP.a_x*T_f/pDMP.tau);

%init. states for position dmp
Tp.y = pDMP.y0;
Tp.z = zeros(1,3);
Tp.x = 1;

%init. RBF states for forces
TF.x = 1;
kd = 0.001;
Fd = [-10 -10 -10]; % [N]

%%
i=1;
%% Integrate until final calculated phase is reached
while Tp.x > Xmin
    
    %%Calculate the coupling term 
    % integrate force signal for synchronization with DMP phase
    TF = RBF_int(RBF,TF);
    % Save integrated force trajectory
    iF(i,:) = TF.y;
    %scale coupling term signal 
    Fc = 0.5*TF.y;
    % calculate coupling term
    TF.m = kd*(Fd+Fc);
    %% Integrate position DMP
    [Tp,TF]=pDMP_int(pDMP,Tp,TF);    
    % save integrated trajectory
    xN(i,:) = Tp.y;
    
    i=i+1;
end

%% Plot Position part of the trajectory
grid on
p1=plot(xN,'r');  % plot DMP trajectory
hold on
p2=plot(ppath,'--b'); % plot example trajectory
legend([p1(1),p2(1)],'DMP generated trajectory','Demonstrated trajectory')
figure(2)
p1=plot(iF,'r');  % plot DMP trajectory
hold on
p2=plot(fpath,'--b'); % plot example trajectory
legend([p1(1),p2(1)],'RBF generated trajectory','Recorded force signal')
