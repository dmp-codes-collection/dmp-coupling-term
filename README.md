# dmp-coupling-term

Implementation of the dmp force coupling terms.

# References

the implementation example is based on the following papers:

Kramberger, A., Shahriari, E., Gams, A., Nemec, B., Ude, A. and Haddadin, S., 2018, October. Passivity based iterative learning of admittance-coupled dynamic movement primitives for interaction with changing environments. In 2018 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS) pp. 6023-6028.

Kramberger, A., Gams, A., Nemec, B., Chrysostomou, D., Madsen, O. and Ude, A., 2017. Generalization of orientation trajectories and force-torque profiles for robotic assembly. Robotics and autonomous systems, 98, pp.333-346.

# Note
This code comes WITHOUT ANY WARRANTY.

