function [TF,psi] = RBF_int(RBF,TF)
%% Input 
% RBF --> Radial Basis Function paramters of the encoded signal
% TF  --> Update parameters.

%% init params for target traj. and fitting
NS = size(RBF.w,2); %number of signals

% the weighted sum of the locally weighted regression models
psi=exp(-(TF.x-RBF.c).^2./(2*RBF.sigma))';
for i = 1:NS
    TF.y(i) = sum((RBF.w(:,i)*TF.x).*psi/(sum(psi))); 
end
TF.basis = psi*TF.x;
dx = -RBF.a_x*TF.x/RBF.tau;
TF.x=TF.x+dx*RBF.dt;


