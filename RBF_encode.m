function [RBF]=RBF_encode(y, RBF)
% signal encoding with radial bassis functions

% input measured values 
%   y --> signal
%   RBF --> parameters for encoding

% RBF parameters
%   N --> number of Gaussian kernel functions
%   w --> vector of weights
%   c,
%   sigma
%   tau


%% params - global
[NT,NS] = size(y);

RBF.tau = (NT-1)*RBF.dt;
P = eye(RBF.N)*1000;        % initiate the covariance matrix
RBF.w = zeros(RBF.N,NS);     % initial weights


%% init params for trajectory fitting
x = 1;

%%% gausian kernel functions setup 
c_lin=linspace(0,1,RBF.N);
RBF.c=exp(-RBF.a_x * c_lin);
RBF.sigma=(diff(RBF.c)*0.75).^2;
RBF.sigma=[RBF.sigma,RBF.sigma(end)];

%% Fit all points of the signal, for every DOF e.g. trajectory
for t=1:NT
    %% the weighted sum of the locally weighted regression models
    psi=exp(-0.5*(x-RBF.c).^2./RBF.sigma)';
    %% recursive regression step
    reg = psi*x/sum(psi);
    % calculation of all weights over entire interval x
    P = (P-(P*reg*reg'*P)/(1+reg'*P*reg));
    for i = 1:NS
        e = y(t,i) - reg'*RBF.w(:,i);
        RBF.w(:,i) = RBF.w(:,i) + e*P*reg;
    end
    %% update parameters
    dx = -RBF.a_x*x;
    dx = dx/RBF.tau;
    x = x + dx*RBF.dt;
end    
    

