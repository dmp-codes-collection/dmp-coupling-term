function [pDMP]=pDMPlearn(y, pDMP)

% Locally waighetd regression DMP,  
% input measured values 
%   y,  ... position(t)
%   dy, ... velocity(t)
%   ddy, ... acceleration(t)
%   dt, ... sample time
%   DMP, ... DMP parameters 

% DMP parameters
%   N,  ... number of Gaussian kernel functions
%   w,  ... weight vector of size(Nx1)
%   c,
%   sigma
%   tau
%   a_x
%   a_z


%% params - global
[NT,NS] = size(y);
%Define the initial state of the DMP(y0)- first point of the demonstrated trajectory
pDMP.y0 = y(1,:); 
%Define the goal state of the DMP(goal)- last point of the demonstrated trajecory
pDMP.goal  = y(NT,:);
% Calculate the length of the recorded trajectory
pDMP.tau = (NT-1)*pDMP.dt;
%% Generate derivatives for the entire trajecory
% Sintesized velocities
dy= diff(y)/pDMP.dt;
dy=[zeros(1,NS);dy];
%Sintesized acceleration
ddy= diff(dy)/pDMP.dt;
ddy=[zeros(1,NS);ddy];

% initial value
pDMP.dy0 = dy(1,:);   

%% init params for trajectory fitting
x = 1;
h=-0.5;
dx = 0;

% initialize covariance matix
for i = 1:NS
     P(:,:,i) = eye(pDMP.N)*1000;    
end
% initialize weights for trajectory encoding
pDMP.w = zeros(pDMP.N,NS);

%% Definition of the gausian kernel functions
c_l=linspace(0,1,pDMP.N);
pDMP.c=exp(-pDMP.a_x * c_l);
pDMP.sigma=(diff(pDMP.c)*0.75).^2;
pDMP.sigma=[pDMP.sigma,pDMP.sigma(end)];

%% fit all points of the trajectory
for j=1:NT
    %% Calculate the weighted sum of the locally weighted regression models
    psi=exp(-0.5*(x-pDMP.c).^2./pDMP.sigma)';
    %% derivatives
    dx=-pDMP.a_x*x;
    %% Calculate temporal scaling derivatives/ tau
     dx = dx/pDMP.tau;
    %% Euler integration
    x=x+dx*pDMP.dt;
    
    for k = 1:NS
        %% target for fitting
        ft = (pDMP.tau^2*ddy(j,k) - pDMP.a_z*(pDMP.a_z/4*(pDMP.goal(k)-y(j,k))- pDMP.tau*dy(j,k)));
        %% recursive regression step
        a = psi*x/sum(psi);
        % calculation of all weights over entire interval x
        Pk = P(:,:,k);
        P(:,:,k) = (Pk-(Pk*a*a'*Pk)/(1+a'*Pk*a));
        e = ft - a'*pDMP.w(:,k);
        pDMP.w(:,k) = pDMP.w(:,k) + e*P(:,:,k)*a;
    end
end   


