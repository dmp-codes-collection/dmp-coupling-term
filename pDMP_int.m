function [Tp,TF] = pDMP_int(pDMP,Tp,TF)
% input measured values 
%   Tp --> Integration setup parameters
%   TF --> Calculated coupling term parameters
%   pDMP --> position DMP paameters
% discrete move DMP realization,

%% init params for target traj. integration
NS = size(pDMP.w,2); %number of signals

% phase variable
dx=-1*pDMP.a_x*Tp.x;
dx = dx/pDMP.tau;

Tp.x=Tp.x+dx*pDMP.dt;

% the weighted sum of the locally weighted regression models
psi=exp(-(Tp.x-pDMP.c).^2./(2*pDMP.sigma))';
for i = 1:NS
    fx = sum((pDMP.w(:,i)*Tp.x).*psi/(sum(psi)));
    %% integrate DMP with coupling term added to the goal and velocity 
    dz = pDMP.a_z *(pDMP.a_z/4 *((pDMP.goal(i)+TF.m(i)) - Tp.y(i)) - Tp.z(i)) + fx;
    dy = Tp.z(i)+TF.m(i);
    % temporal scaling
    dc = TF.m(i)/pDMP.tau;
    dz = dz/pDMP.tau;
    dy = dy/pDMP.tau;
    % Euler integration
    Tp.z(i)=Tp.z(i)+dz*pDMP.dt;
    Tp.y(i)=Tp.y(i)+dy*pDMP.dt;
    TF.m(i)=TF.m(i)+dc*pDMP.dt;
    
    
end


